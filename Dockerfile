FROM tecktron/python-bjoern:python-3.12-slim

ENV BJOERN_PORT=9024
ENV S8K_N_HOSTS=100000

RUN pip install falcon==3.1.3 peewee==3.17.1
COPY src/ /app
