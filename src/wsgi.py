from os import environ

from falcon import App
from waitress import serve

from db import DatabaseMiddleware
from s8k import VirtualMachineManagerEndpoint


def make_wsgi_app():
    app = App(
        middleware=[DatabaseMiddleware()],
    )
    app.add_route('/vm', VirtualMachineManagerEndpoint())
    return app


def serve_wsgi_app(app):
    serve(app, port=environ.get('S8K_WEB_PORT', 9024))


application = make_wsgi_app()

if __name__ == '__main__':
    serve(application)
