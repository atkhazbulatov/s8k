from os import environ

from falcon import HTTP_UNPROCESSABLE_ENTITY, HTTP_BAD_REQUEST, HTTP_CONFLICT, HTTP_OK
from peewee import BigIntegerField, IntegerField, TextField, fn

from db import BaseModel


class VirtualMachine(BaseModel):
    vm_id = BigIntegerField(primary_key=True)
    host_id = BigIntegerField()
    ram_gb = IntegerField()
    task = TextField()


class VirtualMachineManagerEndpoint:

    @staticmethod
    def on_post(req, resp):
        vm_id = req.media.get('id')
        ram_gb = req.media.get('size')
        task = req.media.get('task')

        if vm_id is None or ram_gb is None or task is None:
            resp.media = {'result': 'NOT_OK'}
            resp.status = HTTP_BAD_REQUEST
            return

        if ram_gb > 128 or sum(1 for c in bin(ram_gb) if c == '1') != 1:
            resp.media = {'result': 'NOT_OK'}
            resp.status = HTTP_BAD_REQUEST
            return

        if VirtualMachine.get_or_none(VirtualMachine.vm_id == vm_id) is not None:
            resp.media = {'result': 'NOT_OK'}
            resp.status = HTTP_CONFLICT
            return

        existing_host_ids = (
            VirtualMachine
                .select(VirtualMachine.host_id)
                .order_by(VirtualMachine.host_id)
                .group_by(VirtualMachine.host_id)
                .having(fn.Sum(VirtualMachine.ram_gb) <= 128 - ram_gb)
        )

        if existing_host_ids:
            host_id = existing_host_ids[0][0]
        else:
            n = environ.get('S8K_N_HOSTS', 1000)
            top_host_ids = (
                VirtualMachine
                    .select(VirtualMachine.host_id)
                    .order_by(VirtualMachine.host_id.desc())
            )
            if not top_host_ids or top_host_ids[0][0] < n:
                host_id = existing_host_ids[0][0]
            else:
                resp.media = {'result': 'NOT_OK'}
                resp.status = HTTP_UNPROCESSABLE_ENTITY
                return

        VirtualMachine.create(
            vm_id=vm_id,
            host_it=existing_host_ids,
            ram_gb=ram_gb,
            task=task,
        )
        resp.media = {'result': 'OK', 'host_id': host_id}
        resp.status = HTTP_OK
