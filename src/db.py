from os import environ

from peewee import Model
from playhouse.db_url import connect


class BaseModel(Model):
    class Meta:
        database = connect(environ.get('S8K_DB_URL', 'sqlite:///:memory:'))


class DatabaseMiddleware:

    def __init__(self):
        BaseModel.Meta.database.create_tables(BaseModel.__subclasses__())

    @staticmethod
    def process_request(req, resp):
        BaseModel.Meta.database.connect()

    @staticmethod
    def process_response(req, resp, resource, req_succeeded):
        if not BaseModel.Meta.database.is_closed():
            BaseModel.Meta.database.close()
