# Run for development

```shell
pipenv install
pipenv run python src/wsgi.py
```

# Run for production

```shell
docker build . -t atkhazbulatov/s8k:latest
docker run atkhazbulatov/s8k:latest -p 4096:4096
```
